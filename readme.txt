This is a C++ library for esp32 lilygo-ttgo-t-display created by 
Tristan VEYET

Installation
--------------------------------------------------------------------------------

To install this library, just place this entire folder as a subfolder in your
Arduino/lib/targets/libraries folder.

When installed, this library should look like:

Arduino/lib/targets/libraries/TTGO_Display                          (this library's folder)
Arduino/lib/targets/libraries/TTGO_Display/TTGO_Display_Log.cpp     (the library implementation file)
Arduino/lib/targets/libraries/TTGO_Display/TTGO_Display_Log.h       (the library description file)
Arduino/lib/targets/libraries/TTGO_Display/keywords.txt             (the syntax coloring file)
Arduino/lib/targets/libraries/TTGO_Display/examples                 (the examples in the "open" menu)
Arduino/lib/targets/libraries/TTGO_Display/readme.txt               (this file)

Building
--------------------------------------------------------------------------------

After this library is installed, you just have to start the Arduino application.
You may see a few warning messages as it's built.

To use this library in a sketch, go to the Sketch | Import Library menu and
select Test.  This will add a corresponding line to the top of your sketch:
#include <TTGO_Display_Log.h>

To stop using this library, delete that line from your sketch.

Geeky information:
After a successful build of this library, a new file named "TTGO_Display_Log.o" will appear
in "Arduino/lib/targets/libraries/TTGO_Display". This file is the built/compiled library
code.

If you choose to modify the code for this library (i.e. "TTGO_Display_Log.cpp" or "TTGO_Display_Log.h"),
then you must first 'unbuild' this library by deleting the "TTGO_Display_Log.o" file. The
new "TTGO_Display_Log.o" with your code will appear after the next press of "verify"

