#######################################
# Syntax Coloring Map For Test
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################

TTGO_Display_Log	KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################

setup	        KEYWORD2
log	            KEYWORD2
addLineToBuffer	KEYWORD2

#######################################
# Instances (KEYWORD2)
#######################################
TTGO_Display_Log	        KEYWORD2

#######################################
# Constants and variables (LITERAL1)
#######################################
MAX_STRING_SIZE             LITERAL1
MAX_LINE_BUFFER_SIZE        LITERAL1
Title                       LITERAL2
