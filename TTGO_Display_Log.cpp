/*
  TTGO_Display_Log.h - TTGO_Display_Log library - Library for logging on TTDisplay
  Copyright (c) 2020 Tristan VEYET.  All right reserved.
*/
#define DEBUG true // For debbuging
// include arduino API
#include "arduino.h"

// include this library's description file
#include "TTGO_Display_Log.h"

#include <TFT_eSPI.h>
#include "Free_Fonts.h"

#define TEXT_HEIGHT 16 // Height of a text line
#define XMAX 135       // Screen width
#define YMAX 240       // Screen heigth

const int TTGO_Display_Log::MAX_STRING_SIZE = 12; // Max length of a line (in chars)
const int TTGO_Display_Log::MAX_LINE_BUFFER_SIZE = 14; // Max lines to display before scrolling
// #define MAX_STRING_SIZE 12       // Max length of a line (in chars)
// #define MAX_LINE_BUFFER_SIZE 14  // Max lines to display before scrolling
std::vector<String> linesBuffer; // Displayed lines buffer

// Keep track of the drawing x, y coordinate
uint16_t xPos = 0;
uint16_t yPos = YMAX - TEXT_HEIGHT;

// TFT_eSPI tft = TFT_eSPI(XMAX, YMAX);

// Constructor /////////////////////////////////////////////////////////////////
TTGO_Display_Log::TTGO_Display_Log()
{
    // initialize this instance's variables
    TTGO_Display_Log::Title = "Logger";
}

// Public Methods //////////////////////////////////////////////////////////////
void TTGO_Display_Log::setup(void)
{
    // Setup the TFT display
    TTGO_Display_Log::tft.init();
    TTGO_Display_Log::tft.setRotation(0); // Must be setRotation(0) for this sketch to work correctly
    TTGO_Display_Log::tft.fillScreen(TFT_BLACK);

    TTGO_Display_Log::tft.setFreeFont(FM9);

    TTGO_Display_Log::tft.setTextColor(TFT_WHITE, TFT_BLUE);
    TTGO_Display_Log::tft.fillRect(0, 0, XMAX, 16, TFT_BLUE);
    TTGO_Display_Log::tft.drawCentreString(TTGO_Display_Log::Title, 70, 0, 2);
    // TTGO_Display_Log::tft.drawCentreString(" Logger ", 70, 0, 2);
    TTGO_Display_Log::tft.drawString("X", 160, 180, 2);

    // Change colour for scrolling zone text
    TTGO_Display_Log::tft.setTextColor(TFT_WHITE, TFT_BLACK);

    // Initialize Serial for debugging purpose
    if (DEBUG && Serial.baudRate() == 0)
    {
        Serial.begin(115200);
    }
}

void TTGO_Display_Log::log(String data)
{
    // Clear screen
    TTGO_Display_Log::tft.fillRect(0, TEXT_HEIGHT, XMAX, YMAX, TFT_BLACK);
    // Add string to buffer
    // - cut into multiple lines if needed
    int remainChars = data.length();
    int begin = 0;
    int end = MAX_STRING_SIZE;

    while (remainChars > MAX_STRING_SIZE)
    {
        String line = data.substring(begin, end);
        addLineToBuffer(line);
        remainChars -= MAX_STRING_SIZE;
        begin = end;
        end += MAX_STRING_SIZE;
    }
    if (remainChars != 0)
    {
        String line = data.substring(begin);
        addLineToBuffer(line);
    }

    int _yPos = yPos;
    // For each line in buffer display on screen starting from bottom
    for (int i = 0; i < linesBuffer.size(); i++)
    {
        TTGO_Display_Log::tft.drawString(linesBuffer[linesBuffer.size() - 1 - i], xPos, _yPos, GFXFF);
        _yPos -= TEXT_HEIGHT;
    }
}

// Private Methods /////////////////////////////////////////////////////////////
void TTGO_Display_Log::addLineToBuffer(String data)
{
    // - respect the maximum numbre of lines in buffer
    if (linesBuffer.size() < MAX_LINE_BUFFER_SIZE)
    {
        linesBuffer.push_back(data);
    }
    else
    {
        linesBuffer.push_back(data);
        linesBuffer.erase(linesBuffer.begin());
    }
}