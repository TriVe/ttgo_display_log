# TTGO_Display

* Author: Tristan VEYET (https://gitlab.com/TriVe)
* Copyright (C) 2020 Tristan VEYET.
* Released under the MIT license.

TTGO_Display is an arduino library to handle the esp32 lilygo-ttgo-t-display LCD display.

## Description

TODO: TO be completed
It allows you to use the TTGO T-Display screen as a logger

## Installation

Download the ZIP archive (ttps://gitlab.com/TriVe/ttgo_display/zipball/master), and choose "Sketch > Include Library > Add .ZIP Library..." and select the downloaded file.

## Usage

```C++
#include <TTGO_Display_Log.h>
#include <Arduino.h>

TTGO_Display_Log dLog = TTGO_Display_Log();

void setup()
{
    dLog.Title = "My Title";
    dLog.setup();
    dLog.log("Hello world");
    dLog.log("abcdefghijkl");
    dLog.log("ABCDEFGHIJKL");
}

void loop()
{
}
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
