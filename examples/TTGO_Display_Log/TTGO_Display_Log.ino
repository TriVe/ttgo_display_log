// TTGO_Display_Log
// by Tristan VEYET

// Demonstrates how to log on the lilygo-ttgo-t-display screen

// Created 10 May 2020

#include <TTGO_Display_Log.h>
#include <Arduino.h>

TTGO_Display_Log dLog = TTGO_Display_Log();

void setup()
{
    dLog.setup();
    dLog.log("Hello world");
    dLog.log("abcdefghijkl");
    dLog.log("ABCDEFGHIJKL");
}

void loop()
{
    for (int i = 0; i < 40000; i++)
    {
        dLog.log(String(i));
        delay(500);
    }
}

