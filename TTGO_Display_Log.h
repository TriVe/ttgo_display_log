/*
  TTGO_Display_Log.h - TTGO_Display_Log library - Library for logging on TTDisplay
  Copyright (c) 2020 Tristan Veyet.  All right reserved.
*/

// ensure this library description is only included once
#ifndef TTGO_Display_Log_h
#define TTGO_Display_Log_h

#include <Arduino.h>
#include <TFT_eSPI.h>

// library interface description
class TTGO_Display_Log
{
    // user-accessible "public" interface
public:
    TTGO_Display_Log();
    void setup(void);
    void log(String);
    TFT_eSPI tft;
    static const int MYINT;
    static const std::string MYSTR;
    String Title;

    // library-accessible "private" interface
private:
    static const int MAX_STRING_SIZE;
    static const int MAX_LINE_BUFFER_SIZE;
    void addLineToBuffer(String);
};

#endif
